using UnityEngine;
using UnityEngine.SceneManagement;


public class AudioController : MonoBehaviour
{
    public static AudioController instance = null;

    #region referencias unity

    [Header("Zona Tracks")]

    [SerializeField] private AudioSource tracks_source = null;
    [SerializeField] private AudioClip[] tracks = null;

    [Header("Zona Sfx")]

    [SerializeField] private AudioSource[] sfxSource = null;

    [SerializeField] private AudioClip[] sfx = null;



    [Header("Zona Ui sfx")]
    [SerializeField] private AudioSource ui_sfx = null;
    [SerializeField] private AudioClip uiSfx = null;

    [Header("Zona Enemies")]

    [SerializeField] private AudioSource enemiesSource = null;
    [SerializeField] private AudioClip[] enemisSfx = null;


    #endregion

    #region unity methods

    private void Awake()
    {
        Initialize();
        DontDestroyOnLoad(gameObject);

    }


    private void Start()
    {
        SetTrack(0, 0.2f);
    }

    #endregion

    public void PlaySfxSalto(int index, float volume = 0.1f, float pitch = 1.0f, float pan = 0.0f)
    {
        sfxSource[0].volume = volume;
        sfxSource[0].pitch = pitch;
        sfxSource[0].panStereo = pan;
        sfxSource[0].PlayOneShot(sfx[index]);
    }

    public void PlaySfxRayo(int index, float volume = 0.1f, float pitch = 1.0f, float pan = 0.0f)
    {
        if (!sfxSource[1].isPlaying)
        {
            sfxSource[1].volume = volume;
            sfxSource[1].pitch = pitch;
            sfxSource[1].panStereo = pan;
            sfxSource[1].PlayOneShot(sfx[index]);
        }
        else
        {
            return;
        }
    }

    public void PlaySfx_Ui(float volume = 0.1f)
    {
        if (!ui_sfx.isPlaying)
        {
            ui_sfx.volume = volume;
            ui_sfx.PlayOneShot(uiSfx);
        }
    }

    public void PlaySfxEnemies(int index, float volume = 0.1f, float pitch = 1.0f, float pan = 0.0f)
    {
        if (!enemiesSource.isPlaying)
        {
            enemiesSource.volume = volume;
            enemiesSource.pitch = pitch;
            enemiesSource.panStereo = pan;
            enemiesSource.PlayOneShot(enemisSfx[index]);
        }
        else
        {
            return;
        }
    }

    public void SetTrack(int index, float volume)
    {
        tracks_source.clip = tracks[index];
        tracks_source.volume = volume;
        tracks_source.Play();
        tracks_source.loop = true;

    }
    public void VolumeMusic(float value)
    {
        tracks_source.volume = value;
    }

    public void VolumeSfx(float value)
    {
        ui_sfx.volume = value;
        sfxSource[0].volume = value;
        sfxSource[1].volume = value;

    }

    private void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }
}
