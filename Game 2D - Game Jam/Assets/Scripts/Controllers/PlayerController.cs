using UnityEngine;

namespace Game.Entities
{
    public class PlayerController : MonoBehaviour
    {
        #region Referencias Unity
        [Header("Referencias a los Objetos")]


        //[SerializeField] private GameObject prefabBala = null;
        // [SerializeField] private Transform lugarCreaDisparo = null;
        [SerializeField] private LayerMask raycastLayers;
        [SerializeField] private LayerMask meleeLayers;

        [Header("Ref ScriptableObjt")]
        [SerializeField] private PlayerConfiguration playerConfiguration = null;
        [SerializeField] private TagManager tagManager = null;
        #endregion

        #region Private Vars
        private Rigidbody2D rigidBody = null;
        private Vector2 movDiff = Vector2.zero;

        private SkillManager skillManager;
        private PlayerAnimationController animationController;

        private float horizontal = 0.0f;
        private float vertical = 0.0f;
        private float vida = 0.0f;
        private float curTimeShoot = 0.0f;
        private float dashTime = 0.0f;
        private float dashLimitTimer = 0.0f;
        private float meleeTimer = 0.0f;
        private int lastHDirection = 0;

        private bool justJumped = false;
        private bool covering = false;
        #endregion

        #region Unity Methods
        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody2D>();
            skillManager = GetComponent<SkillManager>();
            animationController = GetComponent<PlayerAnimationController>();
            vida = playerConfiguration.stargingHp;
            curTimeShoot = playerConfiguration.timeCreateShooting;
        }

        void Update()
        {
            ReadAxisValues();
            CheckDisparo();
            CheckJump();
            CheckCovering();
            CheckDash();
            CheckMelee();
            JumpHeightControl();



            if (dashTime > 0)
                dashTime -= Time.deltaTime;
            else
                rigidBody.velocity = new Vector2(0, rigidBody.velocity.y);

            if (dashLimitTimer > 0)
                dashLimitTimer -= Time.deltaTime;

            if (meleeTimer > 0)
                meleeTimer -= Time.deltaTime;
        }

        private void FixedUpdate()
        {
            MoverPlayer(horizontal);
        }
        #endregion

        #region Private Methods

        private void ReadAxisValues()
        {
            horizontal = Input.GetAxis(playerConfiguration.horizontal);
            vertical = Input.GetAxis(playerConfiguration.vertical);

            if (horizontal != 0)
            {
                if (horizontal > 0)
                    lastHDirection = 1;
                else
                    lastHDirection = -1;

                animationController.SetDirection(lastHDirection);
            }

            animationController.SetIsJumping(!IsGrounded());

            bool isRunning = (IsGrounded() && horizontal != 0);
            animationController.SetIsRunning(isRunning);

        }

        private void MoverPlayer(float axisH)
        {
            // While covering cannot move
            if (covering)
                axisH = 0;

            MoverPlayerHorizontal(axisH);
        }

        private void MoverPlayerHorizontal(float axis)
        {
            float velocityH = transform.right.x * axis * 1 * Time.deltaTime;

            transform.position += new Vector3(velocityH * playerConfiguration.speed, 0);
        }

        private void CheckDisparo()
        {
            // Checking if shooting is allowed
            if (!skillManager.CanShoot())
                //     return;

                if (Input.GetButton(playerConfiguration.fire))
                {
                    curTimeShoot -= 1.0f;

                    if (curTimeShoot <= 1.0f)
                    {
                        Disparo();
                        curTimeShoot = playerConfiguration.timeCreateShooting;
                    }
                }
        }

        private void Disparo()
        {
            //  GameObject bala = Instantiate(prefabBala);
            animationController.Attack(lastHDirection);
            AudioController.instance.PlaySfxRayo(1, Random.Range(0.3f, 0.5f), Random.Range(0.9f, 1.05f));
            // bala.transform.position = lugarCreaDisparo.position;
            //bala.transform.rotation = transform.rotation;
        }

        private void CheckJump()
        {
            // While covering cannot move
            if (covering)
                return;

            // Jump
            if (Input.GetButtonDown(playerConfiguration.jump) && IsGrounded())
                Jump();


            // Checks if wall jump is allowed
            if (!skillManager.CanWallJump())
                return;

            // Stick to walls
            if (Input.GetButton(playerConfiguration.jump))
            {
                if (!IsGrounded())
                {
                    wallPosition wallPosition = IsOnWall();

                    if (wallPosition == wallPosition.Left || wallPosition == wallPosition.Right)
                    {
                        rigidBody.velocity = Vector2.zero;
                    }
                }

                wallPosition isOnWall = IsOnWall();

                if (isOnWall == wallPosition.Left || isOnWall == wallPosition.Right)
                {
                    animationController.SetIsGrabbing(true);

                    switch (isOnWall)
                    {
                        case PlayerController.wallPosition.Left:
                            lastHDirection = -1;
                            break;
                        case PlayerController.wallPosition.Right:
                            lastHDirection = 1;
                            break;
                    }
                }
                else
                    animationController.SetIsGrabbing(false);
            }


            // Jump after releasing while sticked to wall
            if (Input.GetButtonUp(playerConfiguration.jump) && !IsGrounded() && IsOnWall() != wallPosition.notOnWall)
            {
                animationController.SetIsGrabbing(false);
                WallJump();
            }
        }

        private void Jump()
        {
            rigidBody.AddForce(Vector2.up * playerConfiguration.jumpForce, ForceMode2D.Impulse);
            AudioController.instance.PlaySfxSalto(0, Random.Range(0.05f, 0.1f), Random.Range(0.8f, 1.0f));
            justJumped = true;
        }

        private void WallJump()
        {
            rigidBody.AddForce(Vector2.up * playerConfiguration.jumpForce, ForceMode2D.Impulse);
            AudioController.instance.PlaySfxSalto(0, Random.Range(0.05f, 0.1f), Random.Range(0.8f, 1.0f));
        }

        private void CheckCovering()
        {
            covering = (Input.GetButton(playerConfiguration.cover));
        }

        private bool IsGrounded()
        {
            Vector2 raycastOrigin = transform.position;
            Vector2 raycastDirection = Vector2.down;

            bool isGrounded = Raycast(raycastOrigin, raycastDirection, playerConfiguration.jumpRaycastDistance, raycastLayers);

            return isGrounded;
        }

        enum wallPosition { notOnWall, Left, Right };

        private wallPosition IsOnWall()
        {
            // Checking left wall.
            if (Raycast(transform.position, Vector2.left, playerConfiguration.wallJumpDistance, raycastLayers))
                return wallPosition.Left;
            // Checking right wall.
            else if (Raycast(transform.position, Vector2.right, playerConfiguration.wallJumpDistance, raycastLayers))
                return wallPosition.Right;

            return wallPosition.notOnWall;
        }
        private bool Raycast(Vector2 origin, Vector2 direction, float distance, LayerMask layers)
        {
            RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, layers);
            Debug.DrawRay(origin, direction * distance, Color.red, .2f);

            return hit;
        }

        private RaycastHit2D GetRaycastHit(Vector2 origin, Vector2 direction, float distance, LayerMask layers)
        {
            RaycastHit2D hit = Physics2D.Raycast(origin, direction, distance, layers);
            Debug.DrawRay(origin, direction * distance, Color.red, .2f);

            return hit;
        }

        private void CheckDash()
        {
            if (!skillManager.CanDash())
                return;

            if (Input.GetButtonDown(playerConfiguration.dash))
                Dash();
        }

        private void Dash()
        {
            if (dashLimitTimer > 0)
                return;

            dashLimitTimer = playerConfiguration.dashTimeLimit;
            dashTime = playerConfiguration.startDashTime;
            rigidBody.AddForce(new Vector2(horizontal, 0).normalized * playerConfiguration.dashSpeed, ForceMode2D.Impulse);
        }

        private void JumpHeightControl()
        {
            if (justJumped)
            {
                if (rigidBody.velocity.y <= 0)
                {
                    justJumped = false;
                }
                if (Input.GetButtonUp(playerConfiguration.jump) && rigidBody.velocity.y > 0)
                {
                    this.rigidBody.velocity = new Vector2(this.rigidBody.velocity.x, this.rigidBody.velocity.y / playerConfiguration.jumpReleaseReduction);
                    justJumped = false;
                }
            }
        }

        private void CheckMelee()
        {
            if (Input.GetButtonDown(playerConfiguration.melee) && meleeTimer <= 0)
                Melee();
        }

        private void Melee()
        {
            Debug.Log("melee");

            animationController.Attack(lastHDirection);

            meleeTimer = playerConfiguration.meleeTimeLimit;

            RaycastHit2D hit = GetRaycastHit(transform.position, Vector2.right * lastHDirection, playerConfiguration.meleeDistance, meleeLayers);

            if (hit && hit.transform.CompareTag(tagManager.EnemyTag))
                Destroy(hit.transform.gameObject);
        }

        #endregion
    }
}