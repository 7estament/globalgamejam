using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerAnimationController : MonoBehaviour
{
    [SerializeField] string attackParameter;
    [SerializeField] string directionParameter;
    [SerializeField] string runningParameter;
    [SerializeField] string jumpingParameter;
    [SerializeField] string grabbingParameter;

    private Animator animator = null;
    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    public void Attack(int direction)
    {
        animator.SetInteger(directionParameter, direction);
        animator.SetTrigger(attackParameter);
    }
    public void SetDirection(int direction)
    {
        animator.SetInteger(directionParameter, direction);
    }
    public void SetIsRunning(bool isRunning)
    {
        animator.SetBool(runningParameter, isRunning);
    }
    public void SetIsJumping(bool isJumping)
    {
        animator.SetBool(jumpingParameter, isJumping);
    }

    public void SetIsGrabbing(bool isGrabbing)
    {
        animator.SetBool(grabbingParameter, isGrabbing);
    }
}
