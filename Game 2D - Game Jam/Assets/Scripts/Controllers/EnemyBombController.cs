using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyBombController : MonoBehaviour
{
    Transform playerTransform = null;
    Animator animator = null;

    [SerializeField] EnemyConfiguration enemyConfiguration = null;

    private void Awake()
    {
        animator = GetComponent<Animator>();
    }

    private void Start()
    {
        playerTransform = PlayerSingleton.Instance.transform;
    }
    private void Update()
    {
        CheckPlayerRange();
    }

    private void CheckPlayerRange()
    {
        Vector2 dist = playerTransform.position - transform.position;



        Debug.DrawRay(transform.position, dist.normalized * enemyConfiguration.attackDistance, Color.red);

        if (dist.magnitude < enemyConfiguration.viewRange)
        {
            MoveTowardsPlayer(dist);
            animator.SetBool("Walk", true);
        }
        else
            animator.SetBool("Walk", false);


    }

    private void MoveTowardsPlayer(Vector2 distance)
    {
        distance *= Vector2.right;

        transform.position += (Vector3)distance.normalized * 2 * Time.deltaTime;

        if (distance.x > 0)
            animator.SetInteger("Direction", 1);
        else
            animator.SetInteger("Direction", -1);


        if (enemyConfiguration.attackDistance > distance.magnitude)
            Explode();
    }

    private void Explode()
    {
        animator.SetTrigger("Attack");
        AudioController.instance.PlaySfxEnemies(0);
        Destroy(gameObject, enemyConfiguration.destroyTime);
    }
}
