using UnityEngine.SceneManagement;
using TMPro;
using UnityEngine;
using System.Collections;
using UnityEngine.UI;


public class Ui_manager : MonoBehaviour
{
    public static Ui_manager instance = null;

    public GameObject panelPrincipal = null;
    public GameObject optionsPanel = null;
    public GameObject creditsPanel = null;
    public GameObject controlsPanel = null;
    public GameObject soundPanel = null;
    public Slider sliderMusic = null;
    public Slider sliderSfx = null;

    [Header("Seccion Pause")]
    public GameObject panelPausaSprite = null;
    public GameObject panelPausaMenu = null;
    public GameObject panelControlsPause = null;
    public GameObject panelAudioPause = null;

    private bool active = true;

    float valueMusic = 0.5f;
    float valueSfx = 0.3f;
    private void Awake()
    {
        Initialize();
        DontDestroyOnLoad(gameObject);
    }
    private void Update()
    {
        valueMusic = sliderMusic.value;
        valueSfx = sliderSfx.value;

        CargoScene();
        InPuase();
    }

    private void CargoScene()
    {
        if (SceneController.instance.cargoScene == true && active == true)
        {
            panelPausaSprite.SetActive(true);
        }
    }

    public void InPuase()
    {
        if (Input.GetKey(KeyCode.Escape) && SceneController.instance.GetSceneActual()!= 1 )
        {
            panelPausaSprite.SetActive(false);
            AudioController.instance.PlaySfx_Ui(0.4f);
            panelPausaMenu.SetActive(true);
            Time.timeScale = 0;
            active = false;
        }

    }

    public void PauseButton()
    {
        panelPausaSprite.SetActive(false);
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPausaMenu.SetActive(true);
        Time.timeScale = 0;
        active = false;
    }

    public void BackPause()
    {
        panelPausaSprite.SetActive(true);
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPausaMenu.SetActive(false);
        Time.timeScale = 1;
    }


    public void EmpezarJuego()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPrincipal.SetActive(false);
        SceneController.instance.LoadScene(2);
        AudioController.instance.SetTrack(1, 0.2f);
    }

    public void OptionsPanel()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPrincipal.SetActive(false);
        optionsPanel.SetActive(true);
    }

    public void CreditsPanel()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPrincipal.SetActive(false);
        creditsPanel.SetActive(true);
    }

    public void ControlsPanel()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        optionsPanel.SetActive(false);
        controlsPanel.SetActive(true);
    }

    public void SoundPanel()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        optionsPanel.SetActive(false);
        soundPanel.SetActive(true);
    }

    public void AudioPanelPause()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPausaMenu.SetActive(false);
        panelAudioPause.SetActive(true);
    }


    public void MusicGeneralVolumenValue()
    {
        AudioController.instance.VolumeMusic(valueMusic);
    }

    public void SfxGeneralVolumenValue()
    {
        AudioController.instance.VolumeSfx(valueSfx);

        AudioController.instance.PlaySfx_Ui(valueSfx);
    }


    public void BackOptionsPanelDesdeSound()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        soundPanel.SetActive(false);
        optionsPanel.SetActive(true);
    }

    public void BackOptionsPanelDesdeControls()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        controlsPanel.SetActive(false);
        optionsPanel.SetActive(true);
    }


    public void BacknPanelPrincipalDesdeOptions()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPrincipal.SetActive(true);
        optionsPanel.gameObject.SetActive(false);
    }

    public void BacknPanelPrincipalDesdeCredits()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPrincipal.SetActive(true);
        creditsPanel.gameObject.SetActive(false);
    }

    public void SalirDelJuego()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        Application.Quit();
    }

    public void MainMenu()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);

        panelPausaMenu.SetActive(false);
        Time.timeScale = 1;
        active = false;
        SceneController.instance.LoadScene(1);
        AudioController.instance.SetTrack(1, 0.2f);

        panelPrincipal.SetActive(true);

    }

    public void ControlsPause()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPausaMenu.SetActive(false);
        panelControlsPause.SetActive(true);
    }

    public void BackPauseMenu()
    {
        AudioController.instance.PlaySfx_Ui(0.4f);
        panelPausaMenu.SetActive(true);
        panelControlsPause.SetActive(false);
        panelAudioPause.SetActive(false);
    }

    private void Initialize()
    {
        if (instance == null)
        {
            instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
    }

}
