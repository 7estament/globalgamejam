using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneController : MonoBehaviour
{
    [SerializeField] private CanvasGroup canvasGroup = null;
    [SerializeField] private float transitionTime = 1.0f;


    private FloatLerper alphaLerper = null;
    int nextScene = 0;

    public enum STATE {IDLE , OPENING , CLOSING}
    public STATE state = STATE.IDLE;

    public bool cargoScene = false;

    public static SceneController instance = null;


    void Awake()
    {
        Initialize();
        alphaLerper = new FloatLerper(transitionTime, FloatLerper.SMOOTH_TYPE.STEP_SMOOTHER);
    }

    // Update is called once per frame
    void Update()
    {
        UpdateTransition();
    }

    private void  Initialize()
    {
        if (instance == null)
        {
            instance = this;
            DontDestroyOnLoad(gameObject);
        }
        else
        {
            Destroy(gameObject);
        }
    }

    private void UpdateTransition()
    {
        if (!alphaLerper.On)
        {
            return;
        }

        alphaLerper.Update();

        canvasGroup.alpha = alphaLerper.CurrentValue;

        if (alphaLerper.Reached)
        {
            OnReached();
        }
    }
    private void OnReached()
    {
        switch (state)
        {
            case STATE.OPENING:
                OnTransitionEnd();
                break;
            case STATE.CLOSING:
                OnTransitionAtMid();
                break;
        }
    }
    private void OnTransitionStart()
    {
        state = STATE.CLOSING;
        alphaLerper.SetValues(canvasGroup.alpha, 1.0f, true);
        canvasGroup.blocksRaycasts = true;
    }

    private void OnTransitionAtMid()
    {
        state = STATE.OPENING;
        SceneManager.LoadScene(nextScene);
        alphaLerper.SetValues(canvasGroup.alpha, 0.0f, true);
    }
    private void OnTransitionEnd()
    {
        canvasGroup.blocksRaycasts = false;
        state = STATE.IDLE;
        cargoScene = true;
    }
    public void LoadScene(int indexScene)
    {
        nextScene = indexScene;
        cargoScene = false;
        OnTransitionStart();
    }
    public int GetSceneActual()
    {
        return nextScene;
    }
}
