﻿using UnityEngine;
public class FloatLerper
{
    #region private Vars
    private float start;
    private float end;

    private float lerpTime;
    private float currentLerpTime;

    private SMOOTH_TYPE smoothType;
    #endregion

    #region ENUMS
    public enum SMOOTH_TYPE { NONE, EASE_IN, EASE_OUT, EXPONENTIAL, STEP_SMOOTH, STEP_SMOOTHER }
    #endregion

    #region PROPERTIES
    public float CurrentValue { get; protected set; }

    public bool On { get; private set; }

    public bool Reached { get; private set; }
    #endregion

    #region CONSTRUCTORS
    public FloatLerper(float lerpTime, SMOOTH_TYPE smoothType = SMOOTH_TYPE.NONE) 
    {
        this.lerpTime = lerpTime;
        this.smoothType = smoothType;
        Reached = false;
    }

    public FloatLerper(float start, float end, float lerpTime, SMOOTH_TYPE smoothType = SMOOTH_TYPE.NONE)
    {
        this.start = start;
        this.end = end;
        this.lerpTime = lerpTime;
        this.smoothType = smoothType;
        Reached = false;
    }
    #endregion


    public void Update()
    {
        if (!On)
            return;

        currentLerpTime += Time.deltaTime;

        if (currentLerpTime > lerpTime)
        {
            currentLerpTime = lerpTime;
        }

        float perc = currentLerpTime / lerpTime;
        perc = SmoothLerp(perc, smoothType);

        UpdateCurrentPosition(perc);

        if (CheckReached())
        {
            Reached = true;
            SwitchState(false);
        }
    }

    public void SetValues(float start, float end, bool on = false)
    {
        this.start = start;
        this.end = end;
        On = on;
        Reset();
    }

    public void SwitchState(bool on)
    {
        On = on;
    }

    public void SetLerpTime(float time)
    {
        lerpTime = time;
    }

    private void UpdateCurrentPosition(float perc)
    {
        CurrentValue = Mathf.Lerp(start, end, perc);
    }

    private bool CheckReached()
    {
        if (CurrentValue == end)
            return true;
        else
            return false;
    }

    private void Reset()
    {
        currentLerpTime = 0.0f;
        Reached = false;
    }

    private float SmoothLerp(float value, SMOOTH_TYPE mode)
    {
        float smooth = value;

        switch (mode)
        {
            case SMOOTH_TYPE.NONE:
                break;
            case SMOOTH_TYPE.EASE_IN:
                smooth = 1f - Mathf.Cos(value * Mathf.PI * 0.5f);
                break;
            case SMOOTH_TYPE.EASE_OUT:
                smooth = Mathf.Sin(value * Mathf.PI * 0.5f);
                break;
            case SMOOTH_TYPE.EXPONENTIAL:
                smooth = value * value;
                break;
            case SMOOTH_TYPE.STEP_SMOOTH:
                smooth = value * value * (3f - 2f * value);
                break;
            case SMOOTH_TYPE.STEP_SMOOTHER:
                smooth = value * value * value * (value * (6f * value - 15f) + 10f);
                break;
        }

        return smooth;
    }

}
