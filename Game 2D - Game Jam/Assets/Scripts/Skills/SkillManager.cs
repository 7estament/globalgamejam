using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SkillManager : MonoBehaviour
{
    private bool canWallJump = false;
    private bool canShoot = false;
    private bool canDash = false;

    [SerializeField] private string wallJumpObjectTag;
    [SerializeField] private string shootObjectTag;
    [SerializeField] private string dashObjectTag;

    private void OnTriggerEnter2D(Collider2D collision)
    {
        string objectTag = collision.gameObject.tag;

        if (objectTag == wallJumpObjectTag)
            canWallJump = true;
        else if (objectTag == shootObjectTag)
            canShoot = true;
        else if (objectTag == dashObjectTag)
            canDash = true;
    }

    public bool CanWallJump()
    {
        return canWallJump;
    }

    public bool CanShoot()
    {
        return canShoot;
    }

    public bool CanDash()
    {
        return canDash;
    }
}
