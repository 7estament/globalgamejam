using UnityEngine;

namespace Game.Bala
{
    public class AutoDestroy : MonoBehaviour
    {
        [SerializeField] private float timer = 2.0f;

        void Update()
        {
            Destroy(gameObject, timer);
        }

    }
}