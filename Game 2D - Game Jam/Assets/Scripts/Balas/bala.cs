using UnityEngine;

namespace Game.Bala
{
    public class bala : MonoBehaviour
    {
        [SerializeField] private float speedBala = 0.0f;

        private Rigidbody2D rigidBody = null;

        private void Awake()
        {
            rigidBody = GetComponent<Rigidbody2D>();
        }

        private void Start()
        {
            rigidBody.isKinematic = false;

            rigidBody.AddForce(transform.right * speedBala * Time.deltaTime, ForceMode2D.Impulse);
        }


    }

}