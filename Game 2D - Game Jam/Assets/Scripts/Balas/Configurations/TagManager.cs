using UnityEngine;

[CreateAssetMenu(fileName = "TagManager", menuName = "TagManager", order = 0)]

public class TagManager : ScriptableObject
{
    [Header("Tags")]
    public string EnemyTag;
}
