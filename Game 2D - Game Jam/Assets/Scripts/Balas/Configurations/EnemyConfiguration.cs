using UnityEngine;

[CreateAssetMenu(fileName = "Enemy Configuration", menuName = "EnemyConfiguration", order = 0)]

public class EnemyConfiguration : ScriptableObject
{
    [Header("Seccion Vars")]
    public float damage;
    public float viewRange;
    public float attackDistance;
    public float destroyTime;
}
