using UnityEngine;

[CreateAssetMenu(fileName = "Player Configuration" , menuName = "PlayerConfiguration", order = 0)]

public class PlayerConfiguration : ScriptableObject
{
    [Header("Seccion Controles Input")]
    public string horizontal;
    public string vertical;
    public string fire;
    public string jump;
    public string cover;
    public string dash;
    public string melee;


    [Header("Seccion Vars")]
    public float stargingHp;
    public float speed;
    public float dmg;
    public float timeCreateShooting;
    public float jumpForce;
    public float jumpRaycastDistance;
    public float wallJumpDistance;
    public float dashSpeed;
    public float startDashTime;
    public float dashTimeLimit;
    public float meleeTimeLimit;
    public float meleeDistance;
    public float jumpReleaseReduction;
}
